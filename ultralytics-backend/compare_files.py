import sys
from pathlib import Path
from os import path

dir1_list_base = [path.splitext(path.basename(f))[0] for f in Path(sys.argv[1]).iterdir()]
dir2_list_base = [path.splitext(path.basename(f))[0] for f in Path(sys.argv[2]).iterdir()]



for x in dir1_list_base:
    if x not in dir2_list_base:
        print(f"{x} not in dir 2")

for y in dir2_list_base:
    if y not in dir1_list_base:
        print(f"{y} not in dir 1")
