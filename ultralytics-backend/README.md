# RT-DETR ml-backend for Label Studio, utilizing Ultralytics.

Since Ultralytics is licensed AGPL-v3, so is this backend. The boiler

This initial implementation doesn't work with docker-compose, because I found it easier to prototype without needing frequent and time consuming rebuilds. Will move to Docker-compose / podman-compose (and maybe a helm chart, if I feel like it) once feature-complete.

AFAIK, this is the only LS-ml ultralytics backend supporting the latest LS release (12.1), possibly due to API changes? I've gratefully borrowed code for from these repos: [seblful/label-studio-yolov8-backend](https://github.com/seblful/label-studio-yolov8-backend), [35grain/label-studio-yolov8-backend](https://github.com/35grain/label-studio-yolov8-backend), as well as the various example backends provided by Humansignal upstream.



## Current features
- Provides predictions for images
- Saves annotations to a local file for (upcoming) training implementation
- Certain parameters like model's confidence threshold can be adjusted during runtime using "extra parameters" in frontend model connection settings. Extra parameters need to be written as json dict, like `{"confidence_threshold" : 0.4}`.
- Improved support for local image datasets compared to upstream label-studio-ml (actually, only supports this). We'll contribute upstream once our implementation is more stable.
- Useful terminal output for troubleshooting.
- script `symlink_files.py` provided as a manual method for preparing dataset exported from frontend for training. As the name implies, it relies heavily on symlinks for performance and storage reasons. It has a bug where it doesn't symlink corresponding image files for every annotation.txt file, so to avoid messing up the training, non-paired files of each type are deleted before the val/train split. If `compare_files.py` gives no output afterwards, all files are proper.

## Upcoming features
- Containerization
- Support for [Ultralytics' upcoming automodel class](https://github.com/ultralytics/ultralytics/issues/8155), which takes this backend from being RTDTR-only, to broad compatibility with all bounding-box based models supported in the Ultralytics library.
- Training, triggered manually via the frontend "model settings", with automatic preparation of dataset with training and validation split. All important parameters will have sensible defaults, with easy overrides via model settings "extra parameters".



This guide describes the simplest way to start using ML backend with Label Studio.

## Running with Docker (Recommended)

1. Start Machine Learning backend on `http://localhost:9090` with prebuilt image:

```bash
docker-compose up
```

2. Validate that backend is running

```bash
$ curl http://localhost:9090/
{"status":"UP"}
```

3. Connect to the backend from Label Studio running on the same host: go to your project `Settings -> Machine Learning -> Add Model` and specify `http://localhost:9090` as a URL.


## Building from source (Advanced)

To build the ML backend from source, you have to clone the repository and build the Docker image:

```bash
docker-compose build
```

## Running without Docker (Advanced)

To run the ML backend without Docker, you have to clone the repository and install all dependencies using pip:

```bash
python -m venv ml-backend
source ml-backend/bin/activate
pip install -r requirements.txt
```

Then you can start the ML backend:

```bash
label-studio-ml start ./dir_with_your_model
```

# Configuration
Parameters can be set in `docker-compose.yml` before running the container.


The following common parameters are available:
- `BASIC_AUTH_USER` - specify the basic auth user for the model server
- `BASIC_AUTH_PASS` - specify the basic auth password for the model server
- `LOG_LEVEL` - set the log level for the model server
- `WORKERS` - specify the number of workers for the model server
- `THREADS` - specify the number of threads for the model server

# Customization

The ML backend can be customized by adding your own models and logic inside the `./dir_with_your_model` directory. 
