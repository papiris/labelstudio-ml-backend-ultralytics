# This file is part of LS-ultra-backend
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later

from typing import List, Dict, Optional
from label_studio_ml.model import LabelStudioMLBase
from label_studio_ml.response import ModelResponse
from label_studio_ml.utils import get_single_tag_keys, get_image_size
from ultralytics import RTDETR
import os
import sys
from dotenv import load_dotenv
from PIL import Image
from label_studio_tools.core.utils.params import get_env
import requests
import json
from urllib.parse import unquote


load_dotenv('/home/Jacob/git/label-studio-ml-backend/rtdetr-backend/.env')

LABEL_STUDIO_API_KEY = os.getenv('LABEL_STUDIO_API_KEY')
LABEL_STUDIO_HOST = os.getenv('LABEL_STUDIO_HOST')
LOCAL_FILES_DOCUMENT_ROOT = os.getenv('LABEL_STUDIO_LOCAL_FILES_DOCUMENT_ROOT')
MODEL_NAME = 'lynx_rtdetr/train14'
MODEL_INIT = '/home/Jacob/git/farm-animal-detection/models/rtdetr-l.pt'
MODEL_LATEST = f'/home/Jacob/git/farm-animal-detection/{MODEL_NAME}/weights/best.pt'
DETECTION_YAML = '/home/Jacob/git/label-studio-ml-backend/rtdetr-backend/data/server/models/detection.yaml'
STORAGE_DIR = '/home/Jacob/git/label-studio-ml-backend/rtdetr-backend/data'


LOCAL_FILES_DOCUMENT_ROOT_intern = get_env(
    'LOCAL_FILES_DOCUMENT_ROOT', default=os.path.abspath(os.sep)
)


print(f"external doc root: {LOCAL_FILES_DOCUMENT_ROOT}")
print(f"internal doc root: {LOCAL_FILES_DOCUMENT_ROOT_intern}")
print(f"api key used: {LABEL_STUDIO_API_KEY}")
print(f"frontend URL to connect to: {LABEL_STUDIO_HOST}")


class UltralyticsModel(LabelStudioMLBase):
    """Custom ML Backend using Ultralytics framework and RTDETR model
    """
    model = None
    conf_threshold = 0.5
    iou_threshold = 0.3

    def _lazy_init(self):
        if not self.model:

            self.set('modelpath', MODEL_LATEST)
            self.model = RTDETR(self.get('modelpath'))

            self.from_name, self.to_name, self.value, self.labels_in_config = get_single_tag_keys(
                self.parsed_label_config, 'RectangleLabels', 'Image')
            self.labels_in_config = list(self.labels_in_config)
            print(f"getting initialized up by frontend...\n{self.value}\n{self.labels_in_config}\n")

    def setup(self):
        """Configure any parameters of your model here
        """
        # from_name, schema = list(self.parsed_label_config.items())[0]
        # self.from_name = from_name
        # self.to_name = schema['to_name'][0]
        # self.labels = ['lynx', 'sheep']

        self.set("model_version", "lynx_only_nina_0.0.2")
        if self.model:
            print(self.model.info())

    def _get_local_path(self, url):
        """
        more robust implementation of local filepath getter
        """
        print(f"full URL: {url}")
        sub_path = unquote(url.split('?d=')[1])
        print(f"normalized sub path: {sub_path}")

        self.path = LOCAL_FILES_DOCUMENT_ROOT + "/" + sub_path
        if os.path.exists(self.path):
            print(f"got full path:{self.path}")

        else:
            sys.exit(f"path doesn't exist: {self.path}\nquitting backend")

    def predict(self, tasks: List[Dict], context: Optional[Dict] = None, **kwargs) -> ModelResponse:
        """ Write your inference logic here
            :param tasks: [Label Studio tasks in JSON format](https://labelstud.io/guide/task_format.html)
            :param context: [Label Studio context in JSON format](https://labelstud.io/guide/ml_create#Implement-prediction-logic)
            :return model_response
                ModelResponse(predictions=predictions) with
                predictions: [Predictions array in JSON format](https://labelstud.io/guide/export.html#Label-Studio-JSON-format-of-annotated-tasks)
        """
        print(f'''\
        Run prediction on {tasks}
        Received context: {context}
        Project ID: {self.project_id}
        Label config: {self.label_config}
        Parsed JSON Label config: {self.parsed_label_config}
        Extra params: {self.extra_params}''')

        # load model if not already loaded, set params
        self._lazy_init()
        print(self.extra_params)
        if "confidence_threshold" in self.extra_params:
            self.conf_threshold = float(
                self.extra_params["confidence_threshold"]
                )

        url = tasks[0]['data']['image']

        self._get_local_path(url)

        original_width, original_height = get_image_size(self.path)

        # Creating list for predictions and variable for scores
        predictions = []
        score = 0
        i = 0

        # Getting prediction using model
        results = self.model.predict(
            self.path,
            stream=False,
            conf=self.conf_threshold,
            iou=self.iou_threshold
            )
        model_labels = results[0].names
        print(model_labels)
        # Getting boxes from model prediction
        for result in results:
            for i, prediction in enumerate(result.boxes):
                xyxy = prediction.xyxy[0].tolist()

                predictions.append({
                    "id": str(i),
                    "from_name": self.from_name,
                    "to_name": self.to_name,
                    "type": "rectanglelabels",
                    "score": prediction.conf.item(),
                    "original_width": original_width,
                    "original_height": original_height,
                    "image_rotation": 0,
                    "value": {
                        "rotation": 0,
                        "x": xyxy[0] / original_width * 100,
                        "y": xyxy[1] / original_height * 100,
                        "width": (xyxy[2] - xyxy[0]) / original_width * 100,
                        "height": (xyxy[3] - xyxy[1]) / original_height * 100,
                        "rectanglelabels": [self.labels_in_config[int(prediction.cls.item())]]
                    }})
                score += prediction.conf.item()

        score = score / (i + 1)

        final_result = [{
            'result': predictions,
            'score': score / (i + 1)
        }]


        print(f"Mean prediction Score is {score:.3f}.")
        print(predictions)

        return ModelResponse(predictions=final_result)


    def _create_dataset(self, completions: list):
        # prepare storage folder
        label_folder = os.path.join(STORAGE_DIR, 'labels')
        image_folder = os.path.join(LOCAL_FILES_DOCUMENT_ROOT, 'images')
        [os.makedirs(subfolder, exist_ok=True) for subfolder in [label_folder, image_folder]]
        train_images = image_folder + '**.*'

        for i in completions:
            # save image path for list of image path
            data_image = i['data']['image']

            filename = os.path.join(image_folder, os.path.basename(data_image))
            fileext = os.path.splitext(filename)[-1]


            # remove old label file
            label_cache = os.path.join(STORAGE_DIR, 'labels.cache')
            if os.path.exists(label_cache):
                os.remove(label_cache)
                labelname = filename.replace('/images/', '/labels/').replace(fileext, '.txt')
                if os.path.exists(labelname):
                    os.remove(labelname)

            # save annotation for label file
            annotations = i['annotations'][0]['result']
            for result in annotations:
                value = result['value']
                rectanglelabels = self.labels_in_config.index(value['rectanglelabels'][0])

            x = value['x']
            y = value['y']
            width = value['width']
            height = value['height']

            x_center = (x * 2 + width) / 200
            y_center = (y * 2 + height) / 200
            width_box = width / 100
            height_box = height / 100

            with open(labelname, "a") as f:
                f.write(f'{rectanglelabels} {x_center} {y_center} {width_box} {height_box}\n')

    def fit(self, event, data, **kwargs):
        """
        This method is called each time an annotation is created or updated
        You can run your logic here to update the model and persist it to the cache
        It is not recommended to perform long-running operations here, as it will block the main thread
        Instead, consider running a separate process or a thread (like RQ worker) to perform the training
        :param event: event type can be ('ANNOTATION_CREATED', 'ANNOTATION_UPDATED', 'START_TRAINING')
        :param data: the payload received from the event (check [Webhook event reference](https://labelstud.io/guide/webhook_reference.html))
        """

        if event == "ANNOTATION_CREATED" or event == "ANNOTATION_UPDATED":
            print(event)
            print(json.dumps(data, indent=4))

            self._lazy_init()

            task_url = data['task']['data']['image']

            self._get_local_path(task_url)

            filepath = self.path
            filename = filepath.split('/')[-1].split('.jpg')[0]

            label_folder = os.path.join(STORAGE_DIR, 'labels')
            os.makedirs(label_folder, exist_ok=True)

            # save annotation for label file
            annotations = data['annotation']['result']
            for result in annotations:
                value = result['value']
                rectanglelabels = self.labels_in_config.index(value['rectanglelabels'][0])

                x = value['x']
                y = value['y']
                width = value['width']
                height = value['height']

                x_center = (x * 2 + width) / 200
                y_center = (y * 2 + height) / 200
                width_box = width / 100
                height_box = height / 100

                with open(label_folder+"/"+filename+".txt", "a") as f:

                    f.write(f'{rectanglelabels} {x_center} {y_center} {width_box} {height_box}\n')


        # use cache to retrieve the data from the previous fit() runs
        old_data = self.get('my_data')
        old_model_version = self.get('model_version')
        print(f'Old data: {old_data}')
        print(f'Old model version: {old_model_version}')

        # store new data to the cache
        self.set('my_data', 'my_new_data_value')
        self.set('model_version', 'my_new_model_version')
        print(f'New data: {self.get("my_data")}')
        print(f'New model version: {self.get("model_version")}')

        print('fit() completed successfully.')

    def _update_all_annotations(self):
        for i in range(0, 300):
            headers = {"Authorization": f"Token {LABEL_STUDIO_API_KEY}"}

            task_id = i
            response_task = requests.get(f'{LABEL_STUDIO_HOST}/api/tasks/{task_id}', headers=headers)
            jsondata_task = response_task.json()


            filepath = jsondata_task['data']['image'].split('?d=')[1]
            filename = filepath.split('/')[-1].split('.jpg')[0]

            imgpath = LOCAL_FILES_DOCUMENT_ROOT +"/"+ filepath
            print(imgpath)

            label_folder = os.path.join(STORAGE_DIR, 'labels')
            os.makedirs(label_folder, exist_ok=True)

            annotations = jsondata_task['annotations']
            for annotation in annotations:
                value = annotation['result']['value']
                rectanglelabels = self.labels_in_config.index(value['rectanglelabels'][0])

                x = value['x']
                y = value['y']
                width = value['width']
                height = value['height']

                x_center = (x * 2 + width) / 200
                y_center = (y * 2 + height) / 200
                width_box = width / 100
                height_box = height / 100



                with open(label_folder+"/"+filename+".txt", "a") as f:

                    f.write(f'{rectanglelabels} {x_center} {y_center} {width_box} {height_box}\n')
