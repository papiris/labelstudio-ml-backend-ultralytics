import os
import shutil
import random
from pathlib import Path
import filetype

# Set the directories
base_dir = Path("/home/Jacob/git/farm-animal-detection/datasets")
exported_dir = Path("project-1-at-2024-05-15-18-53-8be931d6")

dir_to_link_from = Path(base_dir.joinpath('label-studio-local-dataset'))
dir_to_match = Path(base_dir.joinpath(exported_dir, 'labels'))
dir_to_create_links_in = Path(base_dir.joinpath(exported_dir, 'images'))


# image_extensions = ['.png', '.jpg', '.jpeg', '.webp']

if not dir_to_create_links_in.exists():
    os.makedirs(os.path.dirname(dir_to_create_links_in), exist_ok=True)

files = {}

# Loop through each file and subfolder in dataset folder
for cur_path, dirs, files in dir_to_link_from.walk():
    for filename in files:
        fn = cur_path.joinpath(filename)
        # print(fn)
        if not filetype.is_image(fn):
            continue

        # Check if there is a matching file in dir_to_link_from
        for f in dir_to_match.iterdir():
            if f.stem == fn.stem:
                # print(f"stems: {f.stem}, {fn.stem}")

                # Create the symlink
                try:
                    dir_to_create_links_in.joinpath(fn.name).symlink_to(fn)

                except Exception as e:
                    print(e)




sub_dirs = ['images', 'labels']
traindir = os.path.join(base_dir, exported_dir, 'train')
valdir = os.path.join(base_dir, exported_dir, 'val')

for d in sub_dirs:
    try:
        os.makedirs(os.path.join(traindir, d), exist_ok=True)
    except Exception as e:
        print(e)

    try:
        os.makedirs(os.path.join(valdir, d), exist_ok=True)
    except Exception as e:
        print(e)

txt_list = list(base_dir.joinpath(exported_dir, 'labels').iterdir())
img_list = list(base_dir.joinpath(exported_dir, 'images').iterdir())
print(f"antall annoteringsfiler: {len(txt_list)}")
print(f"antall bildefiler: {len(img_list)}")

num_files_valdir = int(0.1 * len(txt_list))
basenames_files = [f.stem for f in txt_list]
# print(basenames_files)
val_files = basenames_files[:num_files_valdir]
# print(val_files)
k_same = 0
k_not_same = 0




for img_file in img_list:

    if img_file.stem in val_files:
        try:
            new_path = Path(valdir) / 'images' / img_file.name
            img_file.replace(new_path)
        except Exception as e:
            print(e)

    else:
        try:
            new_path = Path(traindir) / 'images' / img_file.name
            img_file.replace(new_path)
        except Exception as e:
            print(e)

for txt_file in txt_list:
    if txt_file.stem not in [img.stem for img in img_list]:
        txt_file.unlink()
    if txt_file.stem in val_files:
        try:
            new_path = Path(valdir) / 'labels' / txt_file.name
            txt_file.replace(new_path)
        except Exception as e:
            print(e)
    else:
        try:
            new_path = Path(traindir) / 'labels' / txt_file.name
            txt_file.replace(new_path)
        except Exception as e:
            print(e)
